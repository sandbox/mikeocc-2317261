#Search Suggestion
This module provides search suggestion feature drawing suggestions from the
Excellence Gateway Read API.  The search suggestion on the Read API returns
suggestions which might be appropriate based on what the user has currently
typed.  These are retrieved by a request _on the Read API_ such as:

      GET /searchsuggest?q=edu&callback=myCallback&maxRows=6

To add this feature to a text field add the `'#autocomplete_path' =>
'eg/searchsuggest'` property and value to the required _textfield_'s properties.

The Drupal menu at `eg/searchsuggest` will return a JSON response in the form
required by Drupal's autocomplete mechanism.  So the request:

        GET /eg/searchsuggest/teac

Will return something along the lines of:

        {
          'teaching': '<div class="eg-search-suggestion eg-search-suggestion-text">teaching</div>
                       <div class="eg-search-suggestion eg-search-suggestion-count">136</div>',
          'teachers': '<div class="eg-search-suggestion eg-search-suggestion-text">teachers</div>
                       <div class="eg-search-suggestion eg-search-suggestion-count">68</div>',
          'teacher': '<div class="eg-search-suggestion eg-search-suggestion-text">teacher</div>
                      <div class="eg-search-suggestion eg-search-suggestion-count">52</div>',
          'teach': '<div class="eg-search-suggestion eg-search-suggestion-text">teach</div>
                    <div class="eg-search-suggestion eg-search-suggestion-count">12</div>'
        }

Note: the above is not valid JSON as the escaping has been removed to aid
readability.

The suggestion text for display includes the count and the suggestion and count
are in separate markup to allow theming.
